<?php

namespace App\Models;

use App\Extensions\DateTime;

class Patient extends BaseModel {

    const ACTIVE   = 0;
    const INACTIVE = 1;

    /**
     * Generated
     */
    protected $table    = 'patient';
    protected $fillable = [
        'id',
        'patientUniqueCode',
        'userId',
        'deleted_at',
        'status',
        'firstName',
        'middleName',
        'lastName'];

}

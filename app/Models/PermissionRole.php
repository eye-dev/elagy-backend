<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\PermissionRole
 *
 * @property int $permission_id
 * @property int $role_id
 * @property-read \App\Models\Hospital $hospital
 * @property-read \App\Models\Permission $permission
 * @property-read \App\Models\Role $role
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionRole wherePermissionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\PermissionRole whereRoleId($value)
 * @mixin \Eloquent
 */
class PermissionRole extends BaseModel {

    /**
     * Generated
     */

    protected $table = 'permission_role';
    protected $fillable = ['permission_id', 'role_id'];


    public function permission() {
        return $this->belongsTo(\App\Models\Permission::class, 'permission_id', 'id');
    }

    public function role() {
        return $this->belongsTo(\App\Models\Role::class, 'role_id', 'id');
    }


}

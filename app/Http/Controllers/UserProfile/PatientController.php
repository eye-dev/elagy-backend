<?php

namespace App\Http\Controllers;

use App\Exceptions\NoImplementationException;
use App\Models\Patient;
use Illuminate\Http\Request;
use Validator;

/**
 * @Controller(prefix="/api/v1/patient")
 * @Resource("/api/v1/patient")
 * @Middleware({"cros", "auth:api", "bindings"})
 */
class PatientController extends Controller {

    protected function validator(array $data, $id = null, $required = 'required') {
        $validator = Validator::make($data, [
            'patientUniqueCode' => 'required|unique:patient,patientUniqueCode,' . $id,
            'fileNo' => 'nullable|integer',
            'birthDate' => 'nullable|date_format:Y-m-d',
            'birthPlace' => 'nullable|string',
            'nationality' => 'nullable|string',
            'parentalConsanguinity' => 'nullable|integer',
            'ageAtDiagnosisNumber' => 'nullable|integer',
            'ageAtDiagnosisType' => 'nullable|integer',
            'profession' => 'nullable|string',
            'contactPhoneNumber' => 'nullable|string',
            'numberOfBrothers' => 'nullable|integer',
            'deficientFactorLevel' => 'nullable|integer',
            'bloodGroup' => 'nullable|integer',
            'gender' => 'nullable|integer',
            'Governorate' => 'nullable|string',
            'city' => 'nullable|string',
            'state' => 'nullable|string',
            'alley' => 'nullable|string',
            'educationalLevel' => 'nullable|integer',
            'schoolAttendance' => 'nullable|integer',
            'schoolPerformance' => 'nullable|integer',
            'fatherName' => 'nullable|string',
            'fatherEducationLevel' => 'nullable|integer',
            'fatherProfession' => 'nullable|string',
            'fatherContactNumber' => 'nullable|string',
            'motherName' => 'nullable|string',
            'motherEducationLevel' => 'nullable|integer',
            'motherProfession' => 'nullable|string',
            'motherContactNumber' => 'nullable|string',
            'userId' => "{$required}|exists:users,id",
            'hospitalId' => "{$required}|exists:hospital,id",
            'firstName' => 'nullable|string',
            'middleName' => 'nullable|string',
            'lastName' => 'nullable|string',


        ]);

        $validator->setAttributeNames([
        ]);

        return $validator;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        throw new NoImplementationException;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        throw new NoImplementationException;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        throw new NoImplementationException;
    }

    /**
     * Display the specified resource.
     *
     * @param  Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function show(Patient $patient) {
        throw new NoImplementationException;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function edit(Patient $patient) {
        throw new NoImplementationException;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Patient $patient) {
        throw new NoImplementationException;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Patient  $patient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Patient $patient) {
        throw new NoImplementationException;
    }

}
